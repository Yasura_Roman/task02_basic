package com.my;

import java.util.Scanner;

/**
 *This class implements the program which can print interval of number
 *and print Fibonacci number by number.
 *
 * @author Roman Yasiura
 * @version 1.0
 * @since 09.11.2019
 */
public final class Main {

    /**
     * This is private constructor of Main class.
     */
    private Main() { }

    /**
     * This is the main method which create dialog
     * with user and use printInterval method
     * and Fibonacci method with user parameter.
     *
     * @param args Unused
     */
    public static void main(final String[] args) {
        Scanner scanner = new Scanner(System.in);
        int start;
        int stop;
        int fibonacci;
        System.out.println("Введіть нижню межу діапазону");
        start = scanner.nextInt();
        System.out.println("Введіть верхню межу діапазону");
        stop = scanner.nextInt();
        printInterval(start, stop);
        System.out.println("Введіть номер числа Фібоначчі");
        fibonacci = scanner.nextInt();
        fibonacci(fibonacci);
    }

    /**
     *This method write to console all even number by order
     * and all odd number order by desc
     * also print count of even and odd number in interval.
     *
     * @param start This is firs number of print interval
     * @param stop This is last number of print interval
     */
    public static void printInterval(final int start, final int stop) {
        int oddCount = 0;
        int evenCount = 0;
        System.out.println();
        if (stop > start) {
            for (int i = start; i <= stop; i++) {
                if (i % 2 == 1) {
                    System.out.print(i + " ");
                    evenCount += 1;
                }
            }
            System.out.println();
            for (int i = stop; i >= start; i--) {
                if (i % 2 == 0) {
                    System.out.print(i + " ");
                    oddCount += 1;
                }
            }
            System.out.println();
            System.out.println("evenCount = " + evenCount
                    + "; oddCount = " + oddCount
            );
        }
    }

    /**
     * This method write to console Fibonacci number.
     *
     * @param size this is number of Fibonacci number
     */
    public static void fibonacci(final int size) {
        final int percent = 100;
        int oddCount = 0; //чи рахується 0 парним?
        int evenCount = 1;
        long[] array = new long[size];
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < size; i++) {
            array[i] =  array[i - 1] + array[i - 2];
            if (i % 2 == 0) {
                oddCount += 1;
            } else {
                evenCount += 1;
            }
        }
        System.out.println("fibonacci " + array[size - 1] + "; odd% = "
                + (Math.round((double) (oddCount * percent) / (double) size))
                + "; even% = "
                + (Math.round((double) (evenCount * percent) / (double) size))
        );
    }

}
